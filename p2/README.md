
# LIS4369 - Extensible Enterprise Solutions
## Sarah Huerta
### Project 2 Requirements:

    1.  Backward engineer dataresults given from P2 Requirements
    2.  Run lis4369.R to produce vaules
    3.  Display graphs and output

#### Assignment Screenshots:

#### Screen Screenshot of R Studio:

| Cars Data Panel |
| ----------------------------------- |
| ![Four Panel Screenshot](img/panel.png) |


#### Screenshot of Cars Graph Output:

|  Cars Graph 1 | Cars Graph 2 |
| -----------------------------------| ----------------------------------- |
| ![Cars Graph 1](img/g1.png) | ![Cars Graph 2](img/g2.png) |

#### Link to Output File

[R Console Output](https://bitbucket.org/sah16m/lis4369/src/master/p2/lis4369_p2_requirements.txt)
