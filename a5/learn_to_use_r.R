library(dplyr)

url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv" 

mtcars <- read.csv(file=url, head=TRUE,sep=",")
df <- data.frame(mtcars)
#1 print all data
mtcars

#2 print first 10
head(mtcars,10)

#3 last 10 records
tail(mtcars,10)

#4 file structure
str(mtcars)

#5 Column Names
names(mtcars)

#6 display 1st record with colm names
head(mtcars,1)

#7 Print second column data
mtcars$mpg

#8 Column name of Cyl
mtcars$cyl

#9 print colum 3,4
mtcars[3,4]

#10 display  cars having greater than 4 cylinders
subset(df, cyl>4)

#11  Display all cars having more than 4 cyl and greater than 5 gears
filter(df, cyl > 4 & gear >= 5)

#12 Display all cars having more than 4 cyl and exactly 4 gears
filter(df, cyl > 4 & gear == 4)

#13  Display all cars having more than 4 cyl OR greater than 5 gears
filter(df, cyl > 4 | gear == 4)

#14  Display all cars having more than 4 cyl and do NOT have 4 years
filter(df, cyl > 4 & gear != 4)

#15 Total number of rows (only the number)
nrow(mtcars)

#16 Total number of columns
length(mtcars)

#17 Total number of dimensions (ie rows and columns)
dim(mtcars)

#18 display data frame structure - sames as info in python
str(df)

#19 Get mean, median, min, max, quan, var, and sd os horsepower, remove any missing values.

mean(mtcars$hp, na.rm=TRUE)
median(mtcars$hp, na.rm=TRUE)
quantile(mtcars$hp, na.rm=TRUE)
min(mtcars$hp, na.rm=TRUE)
max(mtcars$hp, na.rm=TRUE)
var(mtcars$hp, na.rm=TRUE)
sd(mtcars$hp, na.rm=TRUE)

#20 summary() function prints values from above
summary(mtcars$hp, na.rm=TRUE)

#21, QPLOT and PLOT
plot(mtcars$mpg, mtcars$cyl, main="Sarah Huerta: Relationship between MPG and NUmber of Cyl", xlab= "MPG", ylab="Cyl")
barplot(mtcars$wt, mtcars$mpg, main="Sarah Huerta: Relationship between Weight and MPG", xlab="weight", ylab= "mpg")

pie(tablemtcars$cyl, main="Sarah Huerta: Pie Graph Test (This is using Cyl Col"))
